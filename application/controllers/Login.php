<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->load->view('halamanlogin');
    }

    public function proseslogin(){
       $username = $this->input->post('username');
       $password = $this->input->post('password');

       $cek = $this->db->get_where('pengguna',array('username' => $username,'password' => $password));
       $dataLogin = $cek->row();
       if(count($cek->result()) != 1){
            redirect('Login');
       }else{
            $data = array(
                'akses' => $dataLogin->akses,
                'username' => $dataLogin->username,
                'nama_lengkap' => $dataLogin->nama_lengkap,
                'notelp' => $dataLogin->notelp,
            );      
            $this->session->set_userdata($data);
            redirect('Login/dashboard');
       }
    }

    public function dashboard(){
        $this->load->view('daftarhadir_index');
    }

    public function Logout(){
        $this->session->sess_destroy();
        redirect('Login');
    }
}
