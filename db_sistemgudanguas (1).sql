-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2021 at 11:22 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sistemgudanguas`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `merk` varchar(255) NOT NULL,
  `ukuran` varchar(255) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `warna` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `merk`, `ukuran`, `jenis`, `warna`, `created_at`, `updated_at`) VALUES
(1, 'adidas', '42', 'Guzzle', 'Hijau Lumut', '2021-07-31 07:26:23', NULL),
(2, 'Nike', '42', 'Strip', 'Hitam', '2021-07-31 07:29:21', NULL),
(3, 'Brodo', '42', 'Sepatu Kantor', 'Pink', '2021-07-31 07:30:22', '2021-07-31 07:30:37');

-- --------------------------------------------------------

--
-- Table structure for table `daftarhadir`
--

CREATE TABLE `daftarhadir` (
  `id` int(11) NOT NULL,
  `id_pengguna` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daftarhadir`
--

INSERT INTO `daftarhadir` (`id`, `id_pengguna`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hadir', '2021-07-31 08:15:32', NULL),
(2, 2, 'Sakit', '2021-07-31 08:15:32', NULL),
(3, 3, 'Hadir', '2021-07-31 08:15:32', NULL),
(4, 4, 'Alpha', '2021-07-31 08:15:32', NULL),
(5, 1, 'Hadir', '2021-08-31 08:19:34', '2021-07-31 08:19:49'),
(6, 2, 'Hadir', '2021-08-31 08:19:34', '2021-07-31 08:19:55'),
(7, 3, 'Hadir', '2021-08-31 08:19:34', '2021-07-31 08:20:01'),
(8, 4, 'Hadir', '2021-08-31 08:19:34', '2021-07-31 08:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `notelp` varchar(255) NOT NULL,
  `akses` varchar(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `username`, `password`, `nama_lengkap`, `alamat`, `notelp`, `akses`, `created_at`, `updated_at`) VALUES
(1, 'administrator', '111', 'Hendri Administrator', 'Karawang Kepuh', '080989999', '1', '2021-07-31 04:22:25', NULL),
(2, 'operator', '111', 'Agus Operator', 'Karawang Adiarsa', '080989999', '2', '2021-07-31 04:22:25', NULL),
(3, 'kepalgud', '111', 'Wicaksoni Tabel', 'Karawang TJ 1', '080989999', '4', '2021-07-31 07:12:55', '2021-07-31 07:22:49'),
(4, 'penjaga', '111', 'Pepen Jaga', 'Karawang Tempuran', '080989999', '3', '2021-07-31 07:23:55', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daftarhadir`
--
ALTER TABLE `daftarhadir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `daftarhadir`
--
ALTER TABLE `daftarhadir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
